//
//  ViewController.swift
//  SportsStore
//
//  Created by nhatnt on 9/16/18.
//  Copyright © 2018 nhatnt. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var products = [
        Product(name: "Kayak", description: "A boat for one person", category: "Watersports", price: 275.0, stockLevel: 10),
        Product(name: "Lifejacket", description: "Protective and fashionable", category: "Watersports", price: 48.95, stockLevel: 14),
        Product(name: "Corner Flags", description: "A boat for one person", category: "Chess", price:  19.5, stockLevel: 32),
        Product(name: "Soccer Ball", description: "A boat for one person", category: "Soccer", price: 34.95, stockLevel: 1),
        Product(name: "Stadium", description: "A boat for one person", category: "Soccer", price: 79500.0, stockLevel: 4),
        Product(name: "Thinking Cap", description: "A boat for one person", category: "Soccer", price: 16.0, stockLevel: 8),
        Product(name: "Unsteady Chair", description: "A boat for one person", category: "Chess", price: 29.95, stockLevel: 3),
        Product(name: "Human Chess Board", description: "A boat for one person", category: "Chess", price: 75.0, stockLevel: 2),
        Product(name: "Bling-Bling", description: "A boat for one person", category: "Chess", price: 1200.0, stockLevel: 4)]
    
    @IBOutlet weak var totalStockButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.displayTotalStockButton()
        
        print("\(Utils.currencyStringFromNumber(number: 3333))")
    }
    
    @IBAction func stockLevelDidChange(_ sender: Any) {
        if var currentCell = sender as? UIView {
            while (true) {
                currentCell = currentCell.superview!;
                if let cell = currentCell as? ProductTableCell {
                    if let id = cell.productId {
                        var newStockLevel: Int?
                        if let stepper = sender as? UIStepper {
                            newStockLevel = Int(stepper.value)
                        } else if let textfield = sender as? UITextField {
                            if let newValue = Int(textfield.text!) {
                                newStockLevel = newValue
                            }
                        }
                        if let level = newStockLevel {
                            products[id].stockLevel = level;
                            cell.stockStepper.value = Double(level)
                            cell.stockField.text = String(level)
                        }
                    }
                    break
                }
            }
            displayTotalStockButton();
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = products[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell")
            as! ProductTableCell
        cell.nameLabel.text = product.name
        cell.descriptionLabel.text = product.description
        cell.stockStepper.value = Double(product.stockLevel)
        cell.stockField.text = String(product.stockLevel)
        cell.productId = indexPath.row
        return cell
    }

    func displayTotalStockButton() {
        let stockTotal = self.products.reduce((0, 0.0)) { (total, product) -> (Int, Double) in
            return (total.0 + product.stockLevel, total.1 + product.stockValue )
        }
        self.totalStockButton.setTitle("\(stockTotal.0) Products in Stock. "
            + "Total Value: \(Utils.currencyStringFromNumber(number: stockTotal.1))", for: .normal);
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func calculateTax(product: Product) -> Double {
        return product.price * 0.2
    }
    
    func calculateStockValue(products: [Product]) -> Double {
        return products.reduce(0, { (total, item) -> Double in
            total + item.stockValue
        })
    }
}

class ProductTableCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var stockStepper: UIStepper!
    @IBOutlet weak var stockField: UITextField!
    
    var productId: Int?
}
